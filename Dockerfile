ARG TAG
FROM fluent/fluentd:${TAG}

USER root

RUN ["gem", "install", "--no-document", "elasticsearch:7.5.0", \
    "fluent-plugin-elasticsearch:3.8.0", \
    "fluent-plugin-concat:2.4.0", \
    "fluent-plugin-multi-format-parser:1.0.0"]

USER fluent
